package PresentationLayer;

import java.awt.MenuItem;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import BusinessLayer.BaseProduct;
import BusinessLayer.CompositeProduct;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;

public class ChefGraphicalUserInterface extends JFrame implements Observer {
	private JTextArea textArea;

	public ChefGraphicalUserInterface() {
		this.setTitle("Chef View");
		this.setSize(400, 500);
		textArea = new JTextArea();
		JScrollPane tablePanel1 = new JScrollPane(textArea, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.add(tablePanel1);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	public JTextArea getTextArea() {
		return textArea;
	}

	public void setTextArea(JTextArea textArea) {
		this.textArea = textArea;
	}

	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		System.out.println("Got the update");
		Order order = (Order) arg1;
		textArea.append("Notification for Order #" + order.getOrderId() + ":\n");
		ArrayList<BusinessLayer.MenuItem> orderedItems = Restaurant.orders.get(order);
		for (BusinessLayer.MenuItem item : orderedItems) {
			if (item instanceof BaseProduct) {
				textArea.append(orderedItems.indexOf(item) + 1 + ".[BaseProduct] " + item.getName() + "\n");
			}
			if (item instanceof CompositeProduct) {
				textArea.append(
						orderedItems.indexOf(item) + 1 + ".[CompositeProduct] " + item.getName() + " contains:\n");
				for (BusinessLayer.MenuItem components : ((CompositeProduct) item).getComponents()) {
					textArea.append(components.getName() + "\n");
				}
			}
		}
		textArea.append("Notification ends here.\n\n");

	}

}
