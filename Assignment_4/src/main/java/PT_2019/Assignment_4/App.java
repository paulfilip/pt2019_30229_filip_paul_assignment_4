package PT_2019.Assignment_4;

import BusinessLayer.BaseProduct;
import BusinessLayer.CompositeProduct;
import BusinessLayer.Restaurant;
import PresentationLayer.AdministratorGraphicalUserInterface;
import PresentationLayer.ChefGraphicalUserInterface;
import PresentationLayer.WaiterGraphicalUserInterface;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	Restaurant rest = new Restaurant();
        AdministratorGraphicalUserInterface adminView = new AdministratorGraphicalUserInterface();
        ChefGraphicalUserInterface chefView = new ChefGraphicalUserInterface();
        WaiterGraphicalUserInterface waiterView = new WaiterGraphicalUserInterface(chefView);
    }
}
