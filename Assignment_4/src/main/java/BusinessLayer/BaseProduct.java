package BusinessLayer;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class BaseProduct extends MenuItem implements Serializable {
	private int id;
	private String name;
	private float price;
	private ArrayList<MenuItem> components = new ArrayList<MenuItem>();
	
	public BaseProduct(int id, String name, float price) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
	}
	
	@Override
	public void computePrice() {
		// TODO Auto-generated method stub
	}
	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return id;
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}
	@Override
	public float getPrice() {
		// TODO Auto-generated method stub
		return price;
	}
	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		this.name = name;
		
	}
	@Override
	public void setPrice(float price) {
		// TODO Auto-generated method stub
		this.price = price;
	}

	@Override
	public String toString() {
		return "Produs Simplu ["+name+" - "+price+"]";
	}
	
	
}
