package BusinessLayer;

import java.util.ArrayList;
import java.util.Date;

public interface RestaurantProcessing {
	/**
	 * creates a new BaseProduct based on parameters id, name and price
	 * 
	 * @pre id >= 0
	 * @pre !name.equals("")
	 * @pre price > 0
	 * @post @forall k : [0 .. Restaurant.restaurantMenu.size() - 2] @
	 *       (Restaurant.restaurantMenu.get(k)@pre ==
	 *       Restaurant.restaurantMenu.get(k))
	 * @post Restaurant.restaurantMenu.size() ==
	 *       Restaurant.restaurantMenu.size()@pre + 1
	 * @post @return != null
	 * @post @return.equals(Restaurant.restaurantMenu.get(Restaurant.restaurantMenu.size()@pre))
	 * @invariant isWellFormed()
	 */
	public BaseProduct createNewBaseProduct(int id, String name, float price);

	/**
	 * creates a new CompositeProduct based on parameters id, name and price
	 * 
	 * @pre id >= 0
	 * @pre !name.equals("")
	 * @post Restaurant.restaurantMenu.size() ==
	 *       Restaurant.restaurantMenu.size()@pre + 1
	 * @post @forall k : [0 .. Restaurant.restaurantMenu.size() - 2] @
	 *       (Restaurant.restaurantMenu.get(k)@pre ==
	 *       Restaurant.restaurantMenu.get(k))
	 * @post @return != null
	 * @post @return.equals(Restaurant.restaurantMenu.get(Restaurant.restaurantMenu.size()@pre))
	 * @invariant isWellFormed()
	 */

	public CompositeProduct createNewCompositeProduct(int id, String name, ArrayList<MenuItem> items);

	/**
	 * deletes a menu item form restaurant menu
	 * @pre item!=null
	 * @pre restaurantMenu.contains(item) == true
	 * @post restaurantMenu.size() == restaurantMenu.size()@pre - 1
	 * @post !restaurantMenu.contains(item)
	 * @invariant isWellFormed()
	 * 
	 */
	public void deleteMenuItem(MenuItem item);

	/**
	 * @pre id >= 0
	 * @pre !name.equals("")
	 * @pre price > 0
	 * @post @forall k [0 .. Restaurant.restaurantMenu.size() - 1] @ (k!=id ==>
	 *       Restaurant.restaurantMenu.get(k)@pre ==
	 *       Restaurant.restaurantMenu.get(k) )
	 * @post restaurant.RestaurantMenu.size() ==
	 *       restaurant.RestaurantMenu.size()@pre
	 * @invariant isWellFormed()
	 */
	public void editBaseProduct(int id, String name, float price);

	/**
	 * @pre id >= 0
	 * @pre !name.equals("")
	 * @pre price > 0
	 * @post @forall k [0 .. Restaurant.restaurantMenu.size() - 1] @ (k!=id ==>
	 *       Restaurant.restaurantMenu.get(k)@pre ==
	 *       Restaurant.restaurantMenu.get(k) )
	 * @post restaurant.RestaurantMenu.size() ==
	 *       restaurant.RestaurantMenu.size()@pre
	 * @invariant isWellFormed()
	 */
	public void editCompositeProduct(int id, String name, ArrayList<MenuItem> components);
	
	/**
	 * creates a new order 
	 * @pre orderId >=0
	 * @pre table > 0
	 * @pre itemsServed != null
	 * @post @return != null
	 * @post Restaurant.orders.containsKey(@return) == true
	 * @post Restaurant.orders.get(@return) != null
	 * @post Restaurant.orders.size() = Restaurant.orders.size()@pre + 1 
	 */
	public Order createNewOrder(int orderId, Date currentDate, int table, ArrayList<MenuItem> itemsServed);

	/**
	 * @pre arg!=null
	 * @pre Restaurant.orders.get(arg) != null
	 * @return > 0
	 */
	public float computePriceOrder(Order arg);

	/**
	 * @pre arg!=null
	 * @pre Restaurant.orders.get(arg) != null
	 */
	public void generateBill(Order arg);
}
