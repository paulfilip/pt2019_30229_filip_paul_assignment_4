package BusinessLayer;

import java.util.ArrayList;

@SuppressWarnings("serial")
public class CompositeProduct extends MenuItem implements java.io.Serializable {
	
	private int id;
	private String name;
	private float price;
	private ArrayList<MenuItem> components;

	public ArrayList<MenuItem> getComponents() {
		return components;
	}

	public void setComponents(ArrayList<MenuItem> components) {
		this.components = components;
	}

	public CompositeProduct(int id, String name) {
		super();
		this.id = id;
		this.name = name;
		this.price = 0.0f;
		this.components = new ArrayList<MenuItem>();
	}

	public CompositeProduct(int id, String name, ArrayList<MenuItem> components) {
		super();
		this.id = id;
		this.name = name;
		this.components = components;
		this.computePrice();
	}

	public void addMenuItem(MenuItem component) {
		components.add(component);
		this.computePrice();
	}

	@Override
	public float getPrice() {
		// TODO Auto-generated method stub
		return price;
	}

	@Override
	public void computePrice() {
		// TODO Auto-generated method stub
		float finalPrice = 0.0f;
		for (MenuItem item : components) {
			finalPrice += item.getPrice();
		}
		price = finalPrice;
	}

	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		this.name = name;
	}

	@Override
	public void setPrice(float price) {
		// TODO Auto-generated method stub
		this.price = price;
	}

	@Override
	public String toString() {
		String result = new String("CompositeProduct [");
		result+= id+" "+name+" "+price+" ";
		for(MenuItem items : components){
			result+=items.toString()+" ";
		}
		return result;
	}
	
}
