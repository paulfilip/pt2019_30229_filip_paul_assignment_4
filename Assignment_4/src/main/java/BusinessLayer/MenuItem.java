package BusinessLayer;

import java.io.Serializable;

public abstract class MenuItem implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6041988930986795685L;
	
	public static int currentId=0;
	
	public abstract int getId();

	public abstract float getPrice();

	public abstract void setPrice(float price);

	public abstract void computePrice();

	public abstract String getName();

	public abstract void setName(String name);
	
}
