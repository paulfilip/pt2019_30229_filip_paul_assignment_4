package DataLayer;

import BusinessLayer.MenuItem;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;

public class RestaurantSerializator {
	public static void serializeMenu(LinkedList<MenuItem> menuList) {
		System.out.println(menuList.size());
		try {
			FileOutputStream fileOut = new FileOutputStream("RestaurantMenu.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeInt(menuList.size());
			if (menuList.size() != 0) {
				for (MenuItem it : menuList) {
					out.writeObject(it);
				}
			}
			out.close();
			fileOut.close();
		} catch (Exception er) {
			System.out.println(er.getMessage());
		}
	}

	public static LinkedList<MenuItem> deserializeMenu() {
		int size = 0;
		MenuItem itemRead = null;
		LinkedList<MenuItem> menuList = new LinkedList<MenuItem>();
		try {
			FileInputStream fileIn = new FileInputStream("RestaurantMenu.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			try{
				size = in.readInt();
				} catch (IOException er){
					size = 0;
				}
			
			System.out.println("The menu has " + size + " items");
			MenuItem.currentId = size;
			for (int i = 0; i < size; i++) {
				itemRead = (MenuItem) in.readObject();
				menuList.add(itemRead);
			}
			in.close();
			fileIn.close();
		} catch(IOException er){
			er.printStackTrace();
		}
			catch (Exception er) {
			System.out.println(er.getMessage());
		}
		return menuList;
	}
}
