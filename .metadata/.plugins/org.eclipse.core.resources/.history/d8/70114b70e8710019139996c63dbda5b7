package BusinessLayer;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Observable;

import DataLayer.RestaurantSerializator;

public class Restaurant extends Observable implements RestaurantProcessing {
	public static HashMap<Order, ArrayList<MenuItem>> orders;
	public static LinkedList<MenuItem> restaurantMenu = new LinkedList<MenuItem>();

	public Restaurant() {
		Restaurant.orders = new HashMap<Order, ArrayList<MenuItem>>();
		Restaurant.restaurantMenu = RestaurantSerializator.deserializeMenu();
	}

	public HashMap<Order, ArrayList<MenuItem>> getOrders() {
		return orders;
	}

	public void setOrders(HashMap<Order, ArrayList<MenuItem>> orders) {
		Restaurant.orders = orders;
	}

	public LinkedList<MenuItem> getRestaurantMenu() {
		return restaurantMenu;
	}

	public void setRestaurantMenu(LinkedList<MenuItem> restaurantMenu) {
		Restaurant.restaurantMenu = restaurantMenu;
	}

	public BaseProduct createNewBaseProduct(int id, String name, float price) {
		// TODO Auto-generated method stub
		int preSize = restaurantMenu.size();
		LinkedList<MenuItem> preList = (LinkedList<MenuItem>) restaurantMenu.clone();
		assert id >= 0;
		assert !name.equals("");
		assert price > 0;

		BaseProduct newProduct = new BaseProduct(id, name, price);
		restaurantMenu.add(newProduct);
		for (int i = 0; i < preSize; i++) {
			assert restaurantMenu.get(i).equals(preList.get(i));
		}

		assert restaurantMenu.size() == preSize + 1;
		assert newProduct != null;
		assert restaurantMenu.get(preSize) == newProduct;

		return newProduct;
	}

	public CompositeProduct createNewCompositeProduct(int id, String name, ArrayList<MenuItem> items) {
		// TODO Auto-generated method stub
		int preSize = restaurantMenu.size();
		LinkedList<MenuItem> preList = (LinkedList<MenuItem>) restaurantMenu.clone();
		assert id >= 0;
		assert !name.equals("");

		CompositeProduct newItem = new CompositeProduct(id, name);
		for (MenuItem mnItem : items) {
			newItem.addMenuItem(mnItem);
		}
		restaurantMenu.add(newItem);
		for (int i = 0; i < preSize; i++) {
			assert restaurantMenu.get(i).equals(preList.get(i));
		}

		assert restaurantMenu.size() == preSize + 1;
		assert newItem != null;
		assert restaurantMenu.get(preSize) == newItem;

		return newItem;
	}

	public void deleteMenuItem(MenuItem item) {
		// TODO Auto-generated method stub
		int preSize = restaurantMenu.size();
		LinkedList<MenuItem> preList = (LinkedList<MenuItem>) restaurantMenu.clone();

		assert item != null;
		assert restaurantMenu.contains(item) == true;
		restaurantMenu.remove(item);
		assert restaurantMenu.size() == preSize - 1;
		assert restaurantMenu.contains(item) == false;
	}

	public void editBaseProduct(int id, String name, float price) {
		// TODO Auto-generated method stub
		int preSize = restaurantMenu.size();
		LinkedList<MenuItem> preList = (LinkedList<MenuItem>) restaurantMenu.clone();
		assert id >= 0;
		assert !name.equals("");
		assert price > 0;

		MenuItem itemToUpdate = null;
		for (MenuItem item : restaurantMenu) {
			if (item.getId() == id) {
				itemToUpdate = item;
				break;
			}
		}
		if (itemToUpdate != null) {
			((BaseProduct) itemToUpdate).setName(name);
			((BaseProduct) itemToUpdate).setPrice(price);
		}

		for (int i = 0; i < restaurantMenu.size(); i++) {
			if (i != id) {
				assert restaurantMenu.get(i) == preList.get(i);
			}
		}

		assert restaurantMenu.size() == preSize;
	}

	public void editCompositeProduct(int id, String name, ArrayList<MenuItem> components) {
		// TODO Auto-generated method stub
		int preSize = restaurantMenu.size();
		LinkedList<MenuItem> preList = (LinkedList<MenuItem>) restaurantMenu.clone();
		assert id >= 0;
		assert !name.equals("");

		MenuItem itemToUpdate = null;
		for (MenuItem item : restaurantMenu) {
			if (item.getId() == id) {
				itemToUpdate = item;
				break;
			}
		}
		if (itemToUpdate != null) {
			((CompositeProduct) itemToUpdate).setName(name);
			((CompositeProduct) itemToUpdate).setComponents(components);
		}

		for (int i = 0; i < restaurantMenu.size(); i++) {
			if (i != id) {
				assert restaurantMenu.get(i) == preList.get(i);
			}
		}

		assert restaurantMenu.size() == preSize;
	}

	public Order createNewOrder(int orderId, Date currentDate, int table, ArrayList<MenuItem> itemsServerd) {
		// TODO Auto-generated method stub
		HashMap<Order, ArrayList<MenuItem>> pre = (HashMap<Order, ArrayList<MenuItem>>) orders.clone();
		int preSize = pre.size();

		assert orderId >= 0;
		assert table > 0;
		assert itemsServerd != null;

		Order newOrder = new Order(orderId, new Date(System.currentTimeMillis()), table);
		orders.put(newOrder, itemsServerd);
		assert newOrder != null;
		assert orders.containsKey(newOrder) == true;
		assert orders.get(newOrder) != null;
		assert orders.size() == preSize + 1;
		return newOrder;

	}

	public float computePriceOrder(Order arg) {
		// TODO Auto-generated method stub
		assert arg!=null;
		assert orders.get(arg)!=null;
		float price = 0.0f;
		ArrayList<MenuItem> itemsServed = orders.get((Order) arg);
		for (MenuItem item : itemsServed) {
			price += item.getPrice();
		}
		assert price > 0;
		return price;
	}

	public void generateBill(Order arg) {
		// TODO Auto-generated method stub
		assert arg!=null;
		FileWriter fileWriter;
		try {
			fileWriter = new FileWriter("Order" + arg.getOrderId() + ".txt");
			@SuppressWarnings("resource")
			PrintWriter printWriter = new PrintWriter(fileWriter);
			ArrayList<MenuItem> itemsForThisOrder = Restaurant.orders.get(arg);
			printWriter.println("Order: " + arg.getOrderId());
			printWriter.println("Date: " + arg.getDate());
			printWriter.println("Table: " + arg.getTable());
			for (MenuItem item : itemsForThisOrder) {
				printWriter.println(item.toString());
			}
			float price = computePriceOrder(arg);
			printWriter.println("Total: " + price);
			printWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
