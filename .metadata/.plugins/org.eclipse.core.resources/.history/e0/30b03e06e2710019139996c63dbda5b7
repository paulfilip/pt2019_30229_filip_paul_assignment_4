package BusinessLayer;

import java.util.ArrayList;
import java.util.Date;

public interface RestaurantProcessing {
	/**
	 * creates a new BaseProduct based on parameters id, name and price
	 * 
	 * @pre id > 0
	 * @pre !name.equals("")
	 * @pre price > 0
	 * @post @forall k : [0 .. Restaurant.restaurantMenu.size() - 2] @ (Restaurant.restaurantMenu.get(k)@pre == Restaurant.restaurantMenu.get(k)) 
	 * @post Restaurant.restaurantMenu.size() == Restaurant.restaurantMenu.size()@pre + 1 
	 * @post @return != null
	 * @post @return.equals(Restaurant.restaurantMenu.get(Restaurant.restaurantMenu.size()@pre))
	 * @invariant isWellFormed()
	 */
	public BaseProduct createNewBaseProduct(int id, String name, float price);

	/**
	 * creates a new CompositeProduct based on parameters id, name and price
	 * 
	 * @pre id > 0
	 * @pre !name.equals("")
	 * @post Restaurant.restaurantMenu.size() == Restaurant.restaurantMenu.size()@pre + 1 
	 * @post @forall k : [0 .. Restaurant.restaurantMenu.size() - 2] @ (Restaurant.restaurantMenu.get(k)@pre == Restaurant.restaurantMenu.get(k)) 
	 * @post @return != null
	 * @post @return.equals(Restaurant.restaurantMenu.get(Restaurant.restaurantMenu.size()@pre))
	 * @invariant isWellFormed()
	 */
	
	public CompositeProduct createNewCompositeProduct(int id, String name, ArrayList<MenuItem> items);

	/**
	 * @pre item!=null
	 * @pre restaurantMenu.contains(item) == true
	 * @post restaurantMenu.size() == restaurantMenu.size()@pre - 1
	 * @post @return != null
	 * @post @return.equals(Restaurant.restaurantMenu.get(Restaurant.restaurantMenu.size()@pre - 1))
	 * @invariant isWellFormed()
	 * 
	 */
	public void deleteMenuItem(MenuItem item);

	/**
	 * @pre id > 0
	 * @pre !name.equals("")
	 * @pre price > 0
	 * @post restaurant.RestaurantMenu.size() == restaurant.RestaurantMenu.size()@pre 
	 * @invariant isWellFormed()
	 */
	public void editBaseProduct(int id, String name, float price);

	/**
	 * 
	 */
	public void editCompositeProduct(int id, String name, ArrayList<MenuItem> components);

	/**
	 * 
	 */
	public Order createNewOrder(int orderId, Date currentDate, int table, ArrayList<MenuItem> itemsServed);

	/**
	 * 
	 */
	public float computePriceOrder(Order arg);

	/**
	 * 
	 */
	public void generateBill(Order arg);
}
