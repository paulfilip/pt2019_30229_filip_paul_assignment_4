package PresentationLayer;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import BusinessLayer.BaseProduct;
import BusinessLayer.CompositeProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;
import BusinessLayer.RestaurantProcessing;
import DataLayer.RestaurantSerializator;

public class AdministratorGraphicalUserInterface extends JFrame implements RestaurantProcessing {
	private JButton addBaseProductBtn = new JButton("Add new Base Product");
	private JButton addCompositeProductBtn = new JButton("Add new Composite Product");
	private JButton deleteMenuItemBtn = new JButton("Delete menu item");
	private JButton editBaseBtn = new JButton("Edit base product");
	private JButton editCompositeBtn = new JButton("Edit composite product");
	private JButton backBtn = new JButton("Clear");
	private JTextField idItemtf = new JTextField(20);
	private JTextField nametf = new JTextField(20);
	private JTextField pricetf = new JTextField(20);
	private JTextField idsCompositetf = new JTextField(20);
	private JPanel panelForTable = new JPanel();
	private JTable menuItemsTable;
	private LinkedList<MenuItem> menuList = Restaurant.restaurantMenu;
	RestaurantSerializator ser = new RestaurantSerializator();

	public AdministratorGraphicalUserInterface() {
		this.setTitle("Administrator View");
		this.setSize(700, 900);
		JPanel content = new JPanel();
		content.setLayout(new GridLayout(3, 1));

		JPanel data = new JPanel();
		data.setLayout(new GridLayout(5, 1));

		JPanel idPanel = new JPanel();
		idPanel.setLayout(new FlowLayout());
		idPanel.add(new JLabel("Introduce ID:"));
		idPanel.add(idItemtf);
		data.add(idPanel);

		JPanel namePanel = new JPanel();
		namePanel.setLayout(new FlowLayout());
		namePanel.add(new JLabel("Introduce name:"));
		namePanel.add(nametf);
		data.add(namePanel);

		JPanel pricePanel = new JPanel();
		pricePanel.setLayout(new FlowLayout());
		pricePanel.add(new JLabel("Introduce price"));
		pricePanel.add(pricetf);
		data.add(pricePanel);

		JPanel compEditPanel = new JPanel();
		compEditPanel.setLayout(new FlowLayout());
		compEditPanel.add(new JLabel("IDs for selected products to add for a new composite product:"));
		compEditPanel.add(idsCompositetf);
		data.add(compEditPanel);

		JPanel buttons = new JPanel();
		buttons.setLayout(new GridLayout(3, 2));
		buttons.add(addBaseProductBtn);
		buttons.add(addCompositeProductBtn);
		buttons.add(editBaseBtn);
		buttons.add(editCompositeBtn);
		buttons.add(deleteMenuItemBtn);
		buttons.add(backBtn);

		menuItemsTable = new JTable();
		JScrollPane tablePanel = new JScrollPane(menuItemsTable, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		panelForTable.add(tablePanel);

		content.add(data);
		content.add(buttons);
		content.add(panelForTable);
		this.repaintTable();
		this.addListeners();

		this.setContentPane(content);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void addListeners() {
		this.addCreateBaseProdcut(new AddBaseProductListener());
		this.addCreateCompositeProduct(new AddCompositeProductListener());
		this.addEditBaseProduct(new EditBaseProductListener());
		this.addEditCompositeProduct(new EditCompositeProductListener());
		this.addDeleteMenuItem(new DeleteMenuItemListener());
		this.addClearListener(new ClearListener());
	}

	public void addCreateBaseProdcut(ActionListener mal) {
		addBaseProductBtn.addActionListener(mal);
	}

	public void addCreateCompositeProduct(ActionListener mal) {
		addCompositeProductBtn.addActionListener(mal);
	}

	public void addEditBaseProduct(ActionListener mal) {
		editBaseBtn.addActionListener(mal);
	}

	public void addEditCompositeProduct(ActionListener mal) {
		editCompositeBtn.addActionListener(mal);
	}

	public void addDeleteMenuItem(ActionListener mal) {
		deleteMenuItemBtn.addActionListener(mal);
	}

	public void addClearListener(ActionListener mal) {
		backBtn.addActionListener(mal);
	}

	public BaseProduct createNewBaseProduct(int id, String name, float price) {
		// TODO Auto-generated method stub
		LinkedList<MenuItem> preList = (LinkedList<MenuItem>) Restaurant.restaurantMenu.clone();
		int preSize = menuList.size();
		BaseProduct newBProduct = null;
		try {

			String nameView = nametf.getText();
			float priceView = Float.parseFloat(pricetf.getText());
			
			assert !nameView.equals("");
			assert priceView > 0;
			
			newBProduct = new BaseProduct(MenuItem.currentId++, nameView, priceView);
			menuList.add(newBProduct);
			System.out.println(newBProduct.toString());
			
			assert menuList.size() == preSize + 1;
			for(int i = 0; i<preSize;i++){
				assert restaurantMenu.get(i).equals(preList.get(i));
			}
			
			this.repaintTable();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		RestaurantSerializator.serializeMenu(menuList);
		return newBProduct;
	}

	public CompositeProduct createNewCompositeProduct(int id, String name, ArrayList<MenuItem> items) {
		// TODO Auto-generated method stub
		try {
			String nameView = nametf.getText();
			CompositeProduct newCProduct = new CompositeProduct(MenuItem.currentId++, nameView);
			String idSComposite = idsCompositetf.getText();
			String[] idIndividual = idSComposite.split(",");
			for (String t : idIndividual) {
				int idView = Integer.parseInt(t);
				for (MenuItem item : menuList) {
					if (item.getId() == idView) {
						newCProduct.addMenuItem(item);
					}
				}
			}
			menuList.add(newCProduct);
			System.out.println(newCProduct.toString());
			this.repaintTable();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		RestaurantSerializator.serializeMenu(menuList);
		return null;
	}

	public void deleteMenuItem(MenuItem item) {
		// TODO Auto-generated method stub
		try {
			int idView = Integer.parseInt(idItemtf.getText());
			MenuItem result = null;
			for (MenuItem items : menuList) {
				if (items.getId() == idView) {
					result = items;
					break;
				}
			}
			if (result != null) {
				menuList.remove(result);
			}
			System.out.println(idView);
			this.repaintTable();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		RestaurantSerializator.serializeMenu(menuList);
	}

	public void editBaseProduct(int id, String name, float price) {
		// TODO Auto-generated method stub
		try {
			int idView = Integer.parseInt(idItemtf.getText());
			String nameView = nametf.getText();
			float priceView = Float.parseFloat(pricetf.getText());
			MenuItem result = null;
			for (MenuItem items : menuList) {
				if (items.getId() == idView) {
					result = items;
					break;
				}
			}
			if (result != null) {
				result.setName(nameView);
				result.setPrice(priceView);
			}

			System.out.println(result.toString());
			this.repaintTable();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		RestaurantSerializator.serializeMenu(menuList);
	}

	public void editCompositeProduct(int id, String name, ArrayList<MenuItem> components) {
		// TODO Auto-generated method stub
		try {
			int idView = Integer.parseInt(idItemtf.getText());
			String nameView = nametf.getText();

			MenuItem result = null;
			for (MenuItem items : menuList) {
				if (items.getId() == idView) {
					result = items;
					break;
				}
			}
			if (result != null) {
				result.setName(nameView);
				((CompositeProduct)result).getComponents().clear();
				
				String idSComposite = idsCompositetf.getText();
				String[] idIndividual = idSComposite.split(",");
				for (String t : idIndividual) {
					int idViewC = Integer.parseInt(t);
					for (MenuItem item : menuList) {
						if (item.getId() == idViewC) {
							((CompositeProduct)result).addMenuItem(item);
						}
					}
				}
			}
			System.out.println(result.toString());
			this.repaintTable();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		RestaurantSerializator.serializeMenu(menuList);
	}

	public Order createNewOrder(int orderId, Date currentDate, int table, ArrayList<MenuItem> itemsServed) {
		// TODO Auto-generated method stub
		return null;
	}

	public float computePriceOrder(Order arg) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void generateBill(Order arg) {
		// TODO Auto-generated method stub

	}

	public void repaintTable() {
		TableGenerator<MenuItem> tableGenerator = new TableGenerator<MenuItem>();

		this.setMenuItemsTable(tableGenerator.createTable(menuList));
	}

	class AddBaseProductListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			createNewBaseProduct(0, new String(" "), 0.0f);
		}

	}

	class AddCompositeProductListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			createNewCompositeProduct(0, new String(" "), new ArrayList<MenuItem>());
		}

	}

	class EditBaseProductListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			editBaseProduct(0, new String(), 0.0f);
		}

	}

	class EditCompositeProductListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			editCompositeProduct(0, new String(), null);
		}

	}

	class DeleteMenuItemListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			deleteMenuItem(null);
		}

	}

	class ClearListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			nametf.setText("");
			idItemtf.setText("");
			pricetf.setText("");
			idsCompositetf.setText("");

		}

	}

	public JTable getMenuItemsTable() {
		return menuItemsTable;
	}

	public void setMenuItemsTable(JTable menuItemsTable) {
		this.getPanelForTable().removeAll();

		this.menuItemsTable = menuItemsTable;

		JScrollPane tablePanel = new JScrollPane(menuItemsTable, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		panelForTable.add(tablePanel);
		panelForTable.revalidate();

	}

	public JPanel getPanelForTable() {
		return panelForTable;
	}

	public void setPanelForTable(JPanel panelForTable) {
		this.panelForTable = panelForTable;
	}

}
